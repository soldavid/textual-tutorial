# Textual Tutorial

[![Python 3.11+](https://img.shields.io/badge/Python-3.11%2B-blue?logo=python&logoColor=yellow)](https://www.python.org/) [![Poetry](https://img.shields.io/endpoint?url=https://python-poetry.org/badge/v0.json)](https://python-poetry.org/) [![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black) [![Checked with mypy](https://www.mypy-lang.org/static/mypy_badge.svg)](https://mypy-lang.org/) [![Ruff](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/astral-sh/ruff/main/assets/badge/v2.json)](https://github.com/astral-sh/ruff) [![security: bandit](https://img.shields.io/badge/security-bandit-yellow.svg)](https://github.com/PyCQA/bandit)

Taken from [the tool's tutorial](https://textual.textualize.io/tutorial/) and the article [Creating a Modal Dialog For Your TUIs in Textual](https://www.blog.pythonlibrary.org/2024/02/06/creating-a-modal-dialog-for-your-tuis-in-textual/).

## Run with console

In one terminal execute the console:

```bash
poetry shell
textual console
```

In another terminal execute the program:

```bash
poetry shell
textual run --dev textual_tutorial/stopwatch.py
```

## Run without the console

Execute in a terminal:

```bash
poetry run python -m textual_tutorial
```

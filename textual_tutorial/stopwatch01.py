from textual.app import App, ComposeResult
from textual.widgets import Footer, Header


class Stopwatch(App):
    """A Textual App to manage stopwatches."""

    BINDINGS = [("d", "toggle_dark", "Toggle dark mode")]

    def compose(self) -> ComposeResult:
        """Create child widgets"""
        yield Header()
        yield Footer()

    def action_toggle_dark(self) -> None:
        """Action to toggle dark mode"""
        self.dark = not self.dark


def main():
    app = Stopwatch()
    app.run()


if __name__ == "__main__":
    main()
